<?php

namespace Drupal\virtual_events\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines Virtual Events Event Entity Form.
 */
class VirtualEventsEventEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $virtual_events_event_entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $virtual_events_event_entity->label(),
      '#description' => $this->t("Label for the Virtual events event entity."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $virtual_events_event_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\virtual_events\Entity\VirtualEventsEventEntity::load',
      ],
      '#disabled' => !$virtual_events_event_entity->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $virtual_events_event_entity = $this->entity;
    $status = $virtual_events_event_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Virtual events event entity.', [
          '%label' => $virtual_events_event_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Virtual events event entity.', [
          '%label' => $virtual_events_event_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($virtual_events_event_entity->toUrl('collection'));
  }

}
