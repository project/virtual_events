<?php

namespace Drupal\virtual_events\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\virtual_events\Entity\VirtualEventsFormatterEntity;

/**
 * Defines Virtual Events Config Entity Form.
 */
class VirtualEventsConfigEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $type = \Drupal::service('plugin.manager.virtual_event_source_plugin');
    $virtualEventsPrePluginManager = \Drupal::service('plugin.manager.virtual_event_pre_handle_plugin');
    $virtualEventsPostPluginManager = \Drupal::service('plugin.manager.virtual_event_post_handle_plugin');

    $postHandlers = $virtualEventsPostPluginManager->getDefinitions();
    $preHandlers = $virtualEventsPrePluginManager->getDefinitions();
    $formBuilder = \Drupal::formBuilder();
    $pluginDefinitions = $type->getDefinitions();
    $pluginConfigForms = [];

    $form = parent::form($form, $form_state);

    $virtual_events_config_entity = $this->entity;

    // Get all entity types and their bundles.
    $allBundles = \Drupal::service('entity_type.bundle.info')->getAllBundleInfo();
    $entityTypeOptions = [];
    foreach ($allBundles as $pkey => $value) {
      $values = [];
      foreach ($value as $key => $value) {
        $values[$key] = $key;
      }
      $entityTypeOptions += [$pkey => $values];
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $virtual_events_config_entity->label(),
      '#description' => $this->t("Label for the this virtual events config."),
      '#required' => TRUE,
    ];

    $form['entityType'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $entityTypeOptions,
      '#default_value' => $virtual_events_config_entity->get('entityType'),
      '#description' => $this->t("Entity type to enable virtual events integration on."),
      '#required' => TRUE,
    ];

    $form['sources'] = [
      '#title' => t('Sources'),
      '#type' => 'details',
      '#description' => t("Enabled Sources"),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#weight' => 100,
    ];

    $sources = $virtual_events_config_entity->get("sources");
    foreach ($pluginDefinitions as $key => $value) {
      $form['sources'][$key] = [
        '#title' => $value["label"],
        '#type' => 'details',
        '#open' => TRUE,
        '#weight' => 100,
      ];
      $form['sources'][$key]["type"] = [
        '#type' => 'value',
        '#value' => $key,
      ];
      $form['sources'][$key]["enabled"] = [
        '#title' => t('Is Enabled'),
        '#type' => 'checkbox',
        '#default_value' => isset($sources[$key]) ? $sources[$key]["enabled"] : FALSE,
      ];
      $form['sources'][$key]['alias'] = [
        '#type' => 'textfield',
        '#title' => t('Alias'),
        '#maxlength' => 255,
        '#default_value' => isset($sources[$key]) ? $sources[$key]["alias"] : $value["label"],
        '#description' => $this->t("Alias for the this virtual events config."),
      ];

      $form['sources'][$key]['description'] = [
        '#type' => 'textarea',
        '#title' => t('Description'),
        '#default_value' => isset($sources[$key]) ? $sources[$key]["description"] : "",
        '#description' => $this->t("Description for the this virtual events config."),
      ];

      $plugin = $type->createInstance($key);
      foreach ($preHandlers as $preHandlerId => $preHandler) {
        if (in_array($id, $preHandler["sourceTypes"])) {
          $preHandlerPlugin = $virtualEventsPrePluginManager->createInstance($preHandlerId);
          $preHandlerPlugin->buildConfigurationForm($form, $form_state, isset($sources[$key]) ? $sources[$key]['data'] : [], $key, $plugin);
        }
        elseif (is_array($preHandler["sourceTypes"]) && empty($preHandler["sourceTypes"])) {
          $preHandlerPlugin = $virtualEventsPrePluginManager->createInstance($preHandlerId);
          $preHandlerPlugin->buildConfigurationForm($form, $form_state, isset($sources[$key]) ? $sources[$key]['data'] : [], $key, $plugin);
        }
      }
      $form['sources'][$key]["data"] = $plugin->buildConfigurationForm($form_state, isset($sources[$key]) ? $sources[$key]['data'] : []);
      foreach ($postHandlers as $postHandlerId => $postHandler) {
        if (in_array($id, $postHandler["sourceTypes"])) {
          $postHandlerPlugin = $virtualEventsPostPluginManager->createInstance($postHandlerId);
          $postHandlerPlugin->buildConfigurationForm($form, $form_state, isset($sources[$key]) ? $sources[$key]['data'] : [], $key, $plugin);
        }
        elseif (is_array($postHandler["sourceTypes"]) && empty($postHandler["sourceTypes"])) {
          $postHandlerPlugin = $virtualEventsPostPluginManager->createInstance($postHandlerId);
          $postHandlerPlugin->buildConfigurationForm($form, $form_state, isset($sources[$key]) ? $sources[$key]['data'] : [], $key, $plugin);
        }
      }
    }

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $virtual_events_config_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\virtual_events\Entity\VirtualEventsConfigEntity::load',
      ],
      '#disabled' => !$virtual_events_config_entity->isNew(),
    ];

    // die(var_dump($form['sources']));.
    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $virtual_events_config_entity = $this->entity;
    $status = $virtual_events_config_entity->save();

    $virtual_events_config_id = $virtual_events_config_entity->id();
    $virtualEventsDefaultFormatter = VirtualEventsFormatterEntity::load("default_display_settings");
    $virtualEventsCurrentFormatter = VirtualEventsFormatterEntity::load($virtual_events_config_id . "_display_settings");

    if (!$virtualEventsCurrentFormatter) {
      $virtualEventsCurrentFormatter = VirtualEventsFormatterEntity::create([
        "id" => $virtual_events_config_id . "_display_settings",
        "label" => $virtual_events_config_entity->label() . " Display Settings",
        "virtualEventsConfigEntity" => $virtual_events_config_id,
        "formatters" => $virtualEventsDefaultFormatter->get("formatters"),
      ]);

      $virtualEventsCurrentFormatter->save();
    }
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Virtual events config entity.', [
          '%label' => $virtual_events_config_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Virtual events config entity.', [
          '%label' => $virtual_events_config_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($virtual_events_config_entity->toUrl('collection'));
  }

}
