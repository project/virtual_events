<?php

namespace Drupal\virtual_events\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VirtualEventsFormatterConfigEntityForm.
 */
class VirtualEventsFormatterEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $virtual_events_formatter_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $virtual_events_formatter_entity->label(),
      '#description' => $this->t("Label for the Virtual events formatter config entity."),
      '#required' => TRUE,
    ];

    $form['virtualEventsConfigEntity'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Virtual Events Config Entity Type'),
      '#default_value' => $virtual_events_formatter_entity->get('virtualEventsConfigEntity'),
      '#value' => $virtual_events_formatter_entity->get('virtualEventsConfigEntity'),
      '#description' => $this->t("Entity type to connect formatter config to"),
      '#required' => TRUE,
    ];

    $virtual_events_config_id = $virtual_events_formatter_entity->get('virtualEventsConfigEntity');
    $config = $this->config('virtual_events.virtualeventsformattersettings');
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $virtualEventsSourcePluginManager = \Drupal::service('plugin.manager.virtual_event_source_plugin');
    $virtualEventsFormatterPluginManager = \Drupal::service('plugin.manager.virtual_event_formatter_plugin');
    $allBundles = \Drupal::service('entity_type.bundle.info')->getAllBundleInfo();
    $formatters = $virtualEventsFormatterPluginManager->getDefinitions();
    $sources = $virtualEventsSourcePluginManager->getDefinitions();
    $virtualEventsPostPluginManager = \Drupal::service('plugin.manager.virtual_event_post_handle_plugin');
    $postHandlers = $virtualEventsPostPluginManager->getDefinitions();

    $form['formatters'] = [
      '#title' => t('Formatters'),
      '#type' => 'details',
      '#description' => t("Available formatters"),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#weight' => 100,
    ];

    $formattersSettings = $virtual_events_formatter_entity->get("formatters");

    foreach ($formatters as $formatterId => $formatter) {
      $formatterPlugin = $virtualEventsFormatterPluginManager->createInstance($formatterId);

      $options = $formattersSettings[$formatterId] ? $formattersSettings[$formatterId]["settings"] : [];
      // Generate the settings form and allow other modules to alter it.
      $settings_form = $formatterPlugin->handleSettingsForm($form_state, NULL, $options);

      if ($settings_form) {
        $settings_form['visible'] = [
          '#title' => t('Show in entity page'),
          '#type' => 'checkbox',
          '#default_value' => $options["visible"] ? TRUE : FALSE,
          '#description' => t('Show or hide the meeting inside enitity view, this is useful if you need to show it only in external block or inside custom twig.'),
        ];

        foreach ($postHandlers as $postHandlerId => $postHandler) {
          if (in_array($id, $postHandler["sourceTypes"])) {
            $postHandlerPlugin = $virtualEventsPostPluginManager->createInstance($postHandlerId);
            $postHandlerPlugin->handleSettingsForm($settings_form, $form_state, NULL, $options, $formatterId, $formatterPlugin);
          }
          elseif (is_array($postHandler["sourceTypes"]) && empty($postHandler["sourceTypes"])) {
            $postHandlerPlugin = $virtualEventsPostPluginManager->createInstance($postHandlerId);
            $postHandlerPlugin->handleSettingsForm($settings_form, $form_state, NULL, $options, $formatterId, $formatterPlugin);
          }
        }
        if ($settings_form) {
          $form['formatters'][$formatterId] = [
            '#title' => $formatter["label"],
            '#type' => 'details',
            '#open' => TRUE,
            '#weight' => 100,
          ];
          $form['formatters'][$formatterId]["type"] = [
            '#type' => 'hidden',
            '#value' => $formatterId,
            '#open' => TRUE,
            '#weight' => 100,
          ];
          $form['formatters'][$formatterId]["settings"] = $settings_form;
        }
      }
    }

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $virtual_events_formatter_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\virtual_events\Entity\VirtualEventsFormatterConfigEntity::load',
      ],
      '#disabled' => !$virtual_events_formatter_entity->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $virtual_events_formatter_entity = $this->entity;
    $status = $virtual_events_formatter_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Virtual events formatter config entity.', [
          '%label' => $virtual_events_formatter_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Virtual events formatter config entity.', [
          '%label' => $virtual_events_formatter_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($virtual_events_formatter_entity->toUrl('collection'));
  }

}
