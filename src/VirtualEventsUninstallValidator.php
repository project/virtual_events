<?php

namespace Drupal\virtual_events;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\virtual_events\Services\VirtualEventsCommonService;
use Drupal\virtual_events\Entity\VirtualEventsConfigEntity;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;

/**
 * Prevents uninstalling while any virtual event entity exist.
 */
class VirtualEventsUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * The virtual events common service.
   *
   * @var \Drupal\virtual_events\Services\VirtualEventsCommonService
   */
  protected $virtualEventsCommonService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new VirtualEventsUninstallValidator.
   *
   * @param \Drupal\virtual_events\Services\VirtualEventsCommonService $virtual_events_common_service
   *   The virtual events common service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(VirtualEventsCommonService $virtual_events_common_service, EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    $this->virtualEventsCommonService = $virtual_events_common_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    if ($module !== "virtual_events") {
      return [];
    }
    $reasons = [];
    $configEntities = VirtualEventsConfigEntity::loadMultiple();
    $eventEntities = VirtualEventsEventEntity::loadMultiple();
    if (count($configEntities) || count($eventEntities)) {
      $reasons[] = $this->t('To uninstall Virtual events module, remove all event entites and event configuration entities');
    }
    return $reasons;
  }

}
