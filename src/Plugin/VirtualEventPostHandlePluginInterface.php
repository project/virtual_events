<?php

namespace Drupal\virtual_events\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Virtual event post handle plugin plugins.
 */
interface VirtualEventPostHandlePluginInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.
}
