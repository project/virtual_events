<?php

namespace Drupal\virtual_events\Plugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\virtual_events\Entity\VirtualEventsConfigEntity;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\virtual_events\Entity\VirtualEventsFormatterEntity;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Base class for Virtual event post handle plugin plugins.
 */
abstract class VirtualEventPostHandlePluginBase extends PluginBase implements VirtualEventPostHandlePluginInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;
  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * Constructs a new VirtualEventPostHandlePluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $string_translation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->stringTranslation = $string_translation;
  }

  /**
   * Creates a new VirtualEventPostHandlePluginBase object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface for service dependency injection.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation')
    );
  }

  /**
   * Handles the plugin configuration form inside the event config entity.
   *
   * @param array $form
   *   The current form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The config entity form state.
   * @param array|null $pluginConfigValues
   *   The config values for the current plugin.
   * @param string $source_type
   *   The source type id that is being processed.
   * @param \Drupal\virtual_events\VirtualEventSourcePluginInterface $plugin
   *   The source plugin instance that is being processed.
   */
  public function buildConfigurationForm(array &$form, FormStateInterface &$form_state, ?array $pluginConfigValues, string $source_type, VirtualEventSourcePluginInterface $plugin) {

  }

  /**
   * Handles the plugin form inside the current entity.
   *
   * @param array $form
   *   The current form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current entity form state.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity|null $event
   *   The created event entity if any.
   * @param array $source_data
   *   The soruce plugin saved data if any.
   * @param string $source_type
   *   The source type id that is being processed.
   * @param \Drupal\virtual_events\VirtualEventSourcePluginInterface $plugin
   *   The source plugin instance that is being processed.
   */
  public function buildEntityForm(array &$form, FormStateInterface &$form_state, ?VirtualEventsEventEntity $event, array $source_data, string $source_type, VirtualEventSourcePluginInterface $plugin) {

  }

  /**
   * Submit handler that will be connected to the current entity form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current entity form state.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsConfigEntity $event_config
   *   The event config entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity|null $event
   *   The event enitity attached to the current entity.
   * @param array $source_config
   *   The source plugin config from the event config entity.
   * @param array $source_data
   *   The source plugin data from the current event entity.
   * @param string $source_type
   *   The source type id that is being processed.
   * @param \Drupal\virtual_events\VirtualEventSourcePluginInterface $plugin
   *   The source plugin instance that is being processed.
   */
  public function handleEntitySubmit(FormStateInterface &$form_state, EntityInterface $entity, VirtualEventsConfigEntity $event_config, ?VirtualEventsEventEntity $event, array $source_config, array $source_data, string $source_type, VirtualEventSourcePluginInterface $plugin) {

  }

  /**
   * Handle extra field structure.
   *
   * @param array $extra
   *   Extra field array.
   * @param string $formatterId
   *   The formatter id that is being processed.
   * @param \Drupal\virtual_events\VirtualEventFormatterPluginInterface $plugin
   *   The formatter plugin instance that is being processed.
   */
  public function handleEntityExtraField(array &$extra, string $formatterId, VirtualEventFormatterPluginInterface $plugin) {

  }

  /**
   * Handle formatter settings form.
   *
   * @param array $form
   *   Formatter Settings From.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current entity form state.
   * @param \Drupal\Core\Entity\Display\EntityDisplayInterface|null $display
   *   The created event entity if any.
   * @param array|null $options
   *   The formatter options stored in the display.
   * @param string $formatterId
   *   The formatter id that is being processed.
   * @param \Drupal\virtual_events\VirtualEventFormatterPluginInterface $plugin
   *   The formatter plugin instance that is being processed.
   */
  public function handleSettingsForm(array &$form, FormStateInterface &$form_state, ?EntityDisplayInterface $display, ?array $options, string $formatterId, VirtualEventFormatterPluginInterface $plugin) {

  }

  /**
   * Handle the display of this formatter inside entity.
   *
   * @param array $element
   *   Element Render array.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity $event
   *   The event enitity attached to the current entity.
   * @param array $source_config
   *   The source plugin config from the event config entity.
   * @param array $source_data
   *   The source plugin data from the current event entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsFormatterEntity $formatters
   *   The formatter config entity attached to the current entity.
   * @param string $formatterId
   *   The formatter id that is being processed.
   * @param \Drupal\virtual_events\VirtualEventFormatterPluginInterface $plugin
   *   The formatter plugin instance that is being processed.
   */
  public function viewElement(array &$element, EntityInterface $entity, VirtualEventsEventEntity &$event, array $source_config, array $source_data, VirtualEventsFormatterEntity $formatters, string $formatterId, VirtualEventFormatterPluginInterface $plugin) {

  }

  /**
   * Handle the display of this formatter inside entity.
   *
   * @param array $build
   *   Entity build array.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity $event
   *   The event enitity attached to the current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsFormatterEntity $formatters
   *   The formatter config entity attached to the current entity.
   */
  public function handleEntityView(array &$build, EntityInterface $entity, VirtualEventsEventEntity $event, VirtualEventsFormatterEntity $formatters) {

  }

}
