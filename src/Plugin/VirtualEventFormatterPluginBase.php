<?php

namespace Drupal\virtual_events\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\virtual_events\Entity\VirtualEventsFormatterEntity;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;


/**
 * Base class for Virtual event formatter plugin plugins.
 */
abstract class VirtualEventFormatterPluginBase extends PluginBase implements VirtualEventFormatterPluginInterface {

  use MessengerTrait;
  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Handle the display of this formatter inside entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity $event
   *   The event enitity attached to the current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsFormatterEntity $formatters_config
   *   The event enitity attached to the current entity.
   * @param array $source_config
   *   The source plugin config from the event config entity.
   * @param array $source_data
   *   The source plugin data from the current event entity.
   * @param array $formatters_settings
   *   The formatter config data from the current event entity.
   *
   * @return array
   *   Render Array.
   */
  final public function render(EntityInterface $entity, VirtualEventsEventEntity $event, VirtualEventsFormatterEntity $formatters_config, array $source_config, array $source_data, array $formatters_settings) {
    $virtualEventsPostPluginManager = \Drupal::service('plugin.manager.virtual_event_post_handle_plugin');
    $postHandlers = $virtualEventsPostPluginManager->getDefinitions();

    $viewElement = $this->viewElement($entity, $event, $formatters_config, $source_config, $source_data, $formatters_settings);
    if ($viewElement) {
      foreach ($postHandlers as $postHandlerId => $postHandler) {
        if (in_array($this->pluginId, $postHandler["sourceTypes"])) {
          $postHandlerPlugin = $virtualEventsPostPluginManager->createInstance($postHandlerId);
          $postHandlerPlugin->viewElement($viewElement, $entity, $event, $source_config, $source_data, $formatters_config, $this->pluginId, $this);
        }
        elseif (is_array($postHandler["sourceTypes"]) && empty($postHandler["sourceTypes"])) {
          $postHandlerPlugin = $virtualEventsPostPluginManager->createInstance($postHandlerId);
          $postHandlerPlugin->viewElement($viewElement, $entity, $event, $source_config, $source_data, $formatters_config, $this->pluginId, $this);
        }
      }
      return $viewElement;
    }
    else {
      return [];
    }

  }

  /**
   * Handle the display of this formatter inside entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity $event
   *   The event enitity attached to the current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsFormatterEntity $formatters_config
   *   The event enitity attached to the current entity.
   * @param array $source_config
   *   The source plugin config from the event config entity.
   * @param array $source_data
   *   The source plugin data from the current event entity.
   * @param array $formatters_settings
   *   The formatter config data from the current event entity.
   *
   * @return array
   *   Render Array.
   */
  abstract public function viewElement(EntityInterface $entity, VirtualEventsEventEntity $event, VirtualEventsFormatterEntity $formatters_config, array $source_config, array $source_data, array $formatters_settings);

  /**
   * Handle Settings Form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current entity form state.
   * @param \Drupal\Core\Entity\Display\EntityDisplayInterface|null $display
   *   The created event entity if any.
   * @param array|null $options
   *   The formatter options stored in the display.
   *
   * @return array
   *   Form array.
   */
  public function handleSettingsForm(FormStateInterface &$form_state, ?EntityDisplayInterface $display, ?array $options) {
    return [];
  }

  /**
   * Handle extra field structure.
   *
   * @return array
   *   Extra field array.
   */
  public function handleEntityExtraField() {
    return [
      'label' => $this->getPluginDefinition()["label"],
      'description' => $this->getPluginDefinition()["label"],
      'weight' => 100,
      'visible' => TRUE,
    ];
  }

  /**
   * Handle the display of this formatter inside entity.
   *
   * @param array $build
   *   Entity build array.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The current enitity display.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity $event
   *   The event enitity attached to the current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsFormatterEntity $formatters_config
   *   The event enitity attached to the current entity.
   * @param array $source_config
   *   The source plugin config from the event config entity.
   * @param array $source_data
   *   The source plugin data from the current event entity.
   * @param array $formatters_settings
   *   The formatter config data from the current event entity.
   */
  public function handleEntityView(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, VirtualEventsEventEntity $event, VirtualEventsFormatterEntity $formatters_config, array $source_config, array $source_data, array $formatters_settings) {
    if ($display->getComponent($this->pluginId)) {
      if ($formatters_settings["visible"]) {
        $build[$this->pluginId] = $this->render($entity, $event, $formatters_config, $source_config, $source_data, $formatters_settings);
        $build[$this->pluginId]['#cache'] = ['max-age' => 0];
      }
    }
  }

}
