<?php

namespace Drupal\virtual_events\Plugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\virtual_events\Entity\VirtualEventsConfigEntity;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\virtual_events\services\VirtualEventsCommonService;
use Drupal\user\UserInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Base class for Virtual event source plugin plugins.
 */
abstract class VirtualEventSourcePluginBase extends PluginBase implements VirtualEventSourcePluginInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;
  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * The virtual events common service.
   *
   * @var \Drupal\virtual_events\services\VirtualEventsCommonService
   */
  protected $virtualEventsCommon;

  /**
   * Constructs a new VirtualEventSourcePluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   * @param \Drupal\virtual_events\services\VirtualEventsCommonService $virtual_events_common
   *   The virtual events common service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $string_translation, VirtualEventsCommonService $virtual_events_common) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->stringTranslation = $string_translation;
    $this->virtualEventsCommon = $virtual_events_common;
  }

  /**
   * Creates a new VirtualEventSourcePluginBase object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface for service dependency injection.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('virtual_events.common')
    );
  }

  /**
   * Handles meeting creation on desired platfrom through apis.
   *
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity $event
   *   The event entity.
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsEventEntity
   *   Virtual Event Entity
   */
  abstract public function createMeeting(VirtualEventsEventEntity $event);

  /**
   * Check if the meeting exists and active on desired platfrom through apis.
   *
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity $event
   *   The event entity.
   *
   * @return bool
   *   check if the virtual event meeting is active or not.
   */
  abstract public function checkMeeting(VirtualEventsEventEntity $event);

  /**
   * Delete meeting on desired platfrom through apis.
   *
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity $event
   *   The event entity.
   *
   * @return bool
   *   if the meeting is deleted or not.
   */
  public function deleteMeeting(VirtualEventsEventEntity $event){
    return true;
  }

  /**
   * Handles the plugin configuration form inside the event config entity.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The config entity form state.
   * @param array|null $pluginConfigValues
   *   The config values for the current plugin.
   */
  abstract public function buildConfigurationForm(FormStateInterface $form_state, ?array $pluginConfigValues);

  /**
   * Handles the plugin form inside the current entity.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current entity form state.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity|null $event
   *   The created event entity if any.
   * @param array $source_data
   *   The soruce plugin saved data if any.
   */
  abstract public function buildEntityForm(FormStateInterface $form_state, ?VirtualEventsEventEntity $event, array $source_data = []);

  /**
   * Submit handler that will be connected to the current entity form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current entity form state.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsConfigEntity $event_config
   *   The event config entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity|null $event
   *   The event enitity attached to the current entity.
   * @param array $source_config
   *   The source plugin config from the event config entity.
   * @param array $source_data
   *   The source plugin data from the current event entity.
   */
  public function handleEntitySubmit(FormStateInterface $form_state, EntityInterface $entity, VirtualEventsConfigEntity $event_config, ?VirtualEventsEventEntity $event, array $source_config, array $source_data) {
    $this->createMeeting($event);
  }

  /**
   * Fucntion that will handle the event registrants.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The event entity.
   * @param \Drupal\user\UserInterface $registrant
   *   The registrant entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsConfigEntity $event_config
   *   The event config entity.
   * @param \Drupal\virtual_events\Entity\VirtualEventsEventEntity|null $event
   *   The event enitity attached to the current entity.
   * @param array $source_config
   *   The source plugin config from the event config entity.
   * @param array $source_data
   *   The source plugin data from the current event entity.
   */
  public function handleRegistration(EntityInterface $entity, UserInterface $registrant, VirtualEventsConfigEntity $event_config, ?VirtualEventsEventEntity $event, array $source_config, array $source_data) {

  }

}
