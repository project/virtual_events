<?php

namespace Drupal\virtual_events\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Virtual event pre handle plugin plugin manager.
 */
class VirtualEventPreHandlePluginManager extends DefaultPluginManager {

  /**
   * Constructs a new VirtualEventPreHandlePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/VirtualEvent/PreHandlePlugin', $namespaces, $module_handler, 'Drupal\virtual_events\Plugin\VirtualEventPreHandlePluginInterface', 'Drupal\virtual_events\Annotation\VirtualEventPreHandlePlugin');

    $this->alterInfo('virtual_events_virtual_event_pre_handle_plugin_info');
    $this->setCacheBackend($cache_backend, 'virtual_events_virtual_event_pre_handle_plugin_plugins');
  }

}
