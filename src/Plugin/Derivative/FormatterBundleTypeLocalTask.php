<?php

namespace Drupal\virtual_events\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Provides dynamic tabs based on node types.
 */
class FormatterBundleTypeLocalTask extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Get node types.
    $entity_types = \Drupal::entityTypeManager()
      ->getStorage('virtual_events_config_entity')
      ->loadMultiple();

    foreach ($entity_types as $entity_type_name => $entity_type) {
      $this->derivatives[$entity_type_name] = $base_plugin_definition;
      $this->derivatives[$entity_type_name]['title'] = $entity_type->label();
    }

    return $this->derivatives;
  }

}
