<?php

namespace Drupal\virtual_events\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Virtual event source plugin plugins.
 */
interface VirtualEventSourcePluginInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.
}
