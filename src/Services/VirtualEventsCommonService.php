<?php

namespace Drupal\virtual_events\Services;

use Drupal\virtual_events\Entity\VirtualEventsConfigEntity;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;

/**
 * Defines Virtual Events Common Service.
 */
class VirtualEventsCommonService {

  /**
   * Constructs a new VirtualEventsCommonService object.
   */
  public function __construct() {

  }

  /**
   * Get event entity by id.
   *
   * @param string $event_id
   *   The event id.
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsEventEntity|bool
   *   The event Entity
   */
  public function getEventById($event_id) {
    $event = VirtualEventsEventEntity::load($event_id);
    return $event;
  }

  /**
   * Get event entity using the referenced entity.
   *
   * @param string $entity_type
   *   Referenced Entity Type.
   * @param string $entity_id
   *   Referenced Entity Id.
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsEventEntity|bool
   *   The event Entity if found or false if not found
   */
  public function getEventByReference($entity_type, $entity_id) {
  
    if (isset($entity_id)) {
      $query = \Drupal::entityQuery('virtual_events_event_entity')
        ->condition('eventEntityReferenceType', $entity_type)
        ->condition('eventEntityReference', $entity_id);

      $entityIds = $query->execute();

      if (!empty($entityIds)) {
        $entities = VirtualEventsEventEntity::loadMultiple($entityIds);

        return array_values($entities)[0];
      } 
    }

    return FALSE;
  }

  /**
   * Get all events config entites (Event Types).
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsConfigEntity[]
   *   Array of meeting config entites.
   */
  public function getAllVirtualEventsTypes() {
    $virtualEventsConfigEntities = VirtualEventsConfigEntity::loadMultiple();
    $types = [];
    foreach ($virtualEventsConfigEntities as $key => $value) {
      if ($value->getEntityType()) {
        $types[$value->get("entityType")] = $value;
      }
    }
    return $types;
  }

  /**
   * Check if entity type is configured as BBB meeting.
   *
   * @param string $entity_type
   *   Refernced Entity Type.
   *
   * @return bool
   *   True if the entity type has been added to meeting types, False otherwise.
   */
  public function isVirtualEventsEvent($entity_type) {
    $types = $this->getAllVirtualEventsTypes();
    return isset($types[$entity_type]);
  }

  /**
   * Get the event config object of a given entity type if found.
   *
   * @param string $entity_type
   *   Refernced Entity Type.
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsConfigEntity|bool
   *   Return Config Entity if found for the given entity type,
   *   ortherwise it will return false
   */
  public function getVirtualEventsConfig($entity_type) {
    // Fetch all available types
    $types = $this->getAllVirtualEventsTypes();
    // Be pesimistic about getting a type
    $type = FALSE;

    if (array_key_exists($entity_type, $types)) {
      $type = $types[$entity_type];
    }

    return $type;

  }

}
