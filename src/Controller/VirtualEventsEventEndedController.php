<?php

namespace Drupal\virtual_events\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\UrlHelper;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines Virtual Events Event Ended Controller.
 */
class VirtualEventsEventEndedController extends ControllerBase {

  /**
   * Reload.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request instance.
   *
   * @return string[]
   *   Return reload render array.
   */
  public function reload(Request $request) {
    $url = $request->query->get('url');
    if($url && !UrlHelper::isValid($url, TRUE)){
      $url = "";
    }
    return [
      '#theme' => 'virtual_events_event_ended',
      '#redirect_to' => $url
    ];
  }

}
