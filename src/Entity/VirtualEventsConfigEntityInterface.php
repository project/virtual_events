<?php

namespace Drupal\virtual_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Virtual events config entity entities.
 */
interface VirtualEventsConfigEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
