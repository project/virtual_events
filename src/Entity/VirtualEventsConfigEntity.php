<?php

namespace Drupal\virtual_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Virtual events config entity entity.
 *
 * @ConfigEntityType(
 *   id = "virtual_events_config_entity",
 *   label = @Translation("Virtual events config entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\virtual_events\VirtualEventsConfigEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\virtual_events\Form\VirtualEventsConfigEntityForm",
 *       "edit" = "Drupal\virtual_events\Form\VirtualEventsConfigEntityForm",
 *       "delete" = "Drupal\virtual_events\Form\VirtualEventsConfigEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\virtual_events\VirtualEventsConfigEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "virtual_events_config_entity",
 *   admin_permission = "manage virtual events config entites",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/virtual_events/virtual_events_config_entity/{virtual_events_config_entity}",
 *     "add-form" = "/admin/virtual_events/virtual_events_config_entity/add",
 *     "edit-form" = "/admin/virtual_events/virtual_events_config_entity/{virtual_events_config_entity}/edit",
 *     "delete-form" = "/admin/virtual_events/virtual_events_config_entity/{virtual_events_config_entity}/delete",
 *     "collection" = "/admin/virtual_events/virtual_events_config_entity"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "entityType",
 *     "sources"
 *   }
 * )
 */
class VirtualEventsConfigEntity extends ConfigEntityBase implements VirtualEventsConfigEntityInterface {

  /**
   * The Virtual events config entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Virtual events config entity label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Virtual events config entity label.
   *
   * @var string
   */
  protected $alias;

  /**
   * The Virtual events config entity label.
   *
   * @var string
   */
  protected $description;

  /**
   * The Virtual events sources config.
   *
   * @var array
   */
  protected $sources;

  /**
   * Get the config array for a specific source.
   *
   * @param string $source_id
   *   The desired source id.
   *
   * @return array
   *   Source config
   */
  public function getSourceConfig($source_id) {
    return isset($this->sources[$source_id]) ? $this->sources[$source_id] : [];
  }

  /**
   * Get the formatters config entity.
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsFormatterEntity
   *   formatters config
   */
  public function getFormattersConfig() {
    $virtualEventsDefaultFormatter = VirtualEventsFormatterEntity::load("default_display_settings");
    $virtualEventsFormatter = VirtualEventsFormatterEntity::load($this->id . "_display_settings");
    if (!$virtualEventsFormatter) {
      return $virtualEventsDefaultFormatter;
    }
    return $virtualEventsFormatter;
  }

}
