<?php

namespace Drupal\virtual_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Virtual events formatter config entity entity.
 *
 * @ConfigEntityType(
 *   id = "virtual_events_formatter_entity",
 *   label = @Translation("Virtual events display settings"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\virtual_events\VirtualEventsFormatterEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\virtual_events\Form\VirtualEventsFormatterEntityForm",
 *       "edit" = "Drupal\virtual_events\Form\VirtualEventsFormatterEntityForm",
 *       "delete" = "Drupal\virtual_events\Form\VirtualEventsFormatterEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\virtual_events\VirtualEventsFormatterEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "virtual_events_formatter_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/virtual_events/virtual_events_formatter_entity/{virtual_events_formatter_entity}",
 *     "add-form" = "/admin/virtual_events/virtual_events_formatter_entity/add",
 *     "edit-form" = "/admin/virtual_events/virtual_events_formatter_entity/{virtual_events_formatter_entity}/edit",
 *     "delete-form" = "/admin/virtual_events/virtual_events_formatter_entity/{virtual_events_formatter_entity}/delete",
 *     "collection" = "/admin/virtual_events/virtual_events_formatter_entity"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "virtualEventsConfigEntity",
 *     "formatters"
 *   }
 * )
 */
class VirtualEventsFormatterEntity extends ConfigEntityBase implements VirtualEventsFormatterEntityInterface {

  /**
   * The Virtual events formatter config entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Virtual events formatter config entity label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Virtual events sources config.
   *
   * @var string
   */
  protected $virtualEventsConfigEntity;

  /**
   * The Virtual events sources config.
   *
   * @var array
   */
  protected $formatters;

}
