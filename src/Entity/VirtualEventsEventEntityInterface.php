<?php

namespace Drupal\virtual_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Virtual events event entity entities.
 */
interface VirtualEventsEventEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
