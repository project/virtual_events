<?php

namespace Drupal\virtual_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Virtual events formatter config entities.
 */
interface VirtualEventsFormatterEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
