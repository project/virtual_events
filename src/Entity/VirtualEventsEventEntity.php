<?php

namespace Drupal\virtual_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Virtual events event entity entity.
 *
 * @ConfigEntityType(
 *   id = "virtual_events_event_entity",
 *   label = @Translation("Virtual events event entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\virtual_events\VirtualEventsEventEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\virtual_events\Form\VirtualEventsEventEntityForm",
 *       "edit" = "Drupal\virtual_events\Form\VirtualEventsEventEntityForm",
 *       "delete" = "Drupal\virtual_events\Form\VirtualEventsEventEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\virtual_events\VirtualEventsEventEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "virtual_events_event_entity",
 *   admin_permission = "access virtual events event entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/virtual_events/virtual_events_event_entity/{virtual_events_event_entity}",
 *     "add-form" = "/admin/virtual_events/virtual_events_event_entity/add",
 *     "edit-form" = "/admin/virtual_events/virtual_events_event_entity/{virtual_events_event_entity}/edit",
 *     "delete-form" = "/admin/virtual_events/virtual_events_event_entity/{virtual_events_event_entity}/delete",
 *     "collection" = "/admin/virtual_events/virtual_events_event_entity"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "eventEntityReferenceType",
 *     "eventEntityReference",
 *     "sources"
 *   }
 * )
 */
class VirtualEventsEventEntity extends ConfigEntityBase implements VirtualEventsEventEntityInterface {

  /**
   * The Virtual events event entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Virtual events event entity label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Virtual events event reference entity type.
   *
   * @var string
   */
  protected $eventEntityReferenceType;

  /**
   * The Virtual events event reference entity id.
   *
   * @var string
   */
  protected $eventEntityReference;

  /**
   * The Virtual events enabled sources.
   *
   * @var array
   */
  protected $sources;

  /**
   * Get the enabled source id.
   *
   * @return string
   *   Source data
   */
  public function getEnabledSourceKey() {
    $sources = $this->get("sources");
    foreach ($sources as $key => $source) {
      if ($source["enabled"]) {
        return $key;
      }
    }
  }

  /**
   * Get the data array for a specific source.
   *
   * @param string $source_id
   *   The desired source id.
   *
   * @return array
   *   Source data
   */
  public function getSourceData($source_id = "") {
    $sources = $this->get("sources");
    if (!isset($sources[$source_id])) {
      return $sources[$this->getEnabledSourceKey()];
    }
    return $sources[$source_id];
  }

  /**
   * Set the data array for a specific source.
   *
   * @param string $source_id
   *   The desired source id.
   * @param array $data
   *   The data array.
   */
  public function setSourceData($source_id, array $data) {
    if (!isset($this->sources[$source_id])) {
      return;
    }
    $this->sources[$source_id] = $data;
  }

  /**
   * Get entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|bool
   *   The entity false if not found
   */
  public function getEntity() {
    $entityType = $this->get('eventEntityReferenceType');
    $entityStorage = \Drupal::entityTypeManager()->getStorage($entityType);
    $entity = $entityStorage->load($this->get('eventEntityReference'));

    return $entity;
  }

  /**
   * Get the event formatter config object of a given event type if found.
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsFormatterEntity
   *   Return formatter Config Entity if found for the given entity type,
   *   ortherwise it will return default formatter config
   */
  public function getVirtualEventsConfigFromattersConfig() {
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $entityType = $this->get('eventEntityReferenceType');
    $entityStorage = \Drupal::entityTypeManager()->getStorage($entityType);
    $entity = $entityStorage->load($this->get('eventEntityReference'));
    $entity_bundle = $entity->bundle();
    $eventConfig = $virtualEventsCommon->getVirtualEventsConfig($entity_bundle);
    return $eventConfig->getFormattersConfig();
  }

  /**
   * Get the event config object of a given event type if found.
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsConfigEntity|bool
   *   Return Config Entity if found for the given entity type,
   *   ortherwise it will return false
   */
  public function getVirtualEventsConfig() {
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $entityType = $this->get('eventEntityReferenceType');
    $entityStorage = \Drupal::entityTypeManager()->getStorage($entityType);
    $entity = $entityStorage->load($this->get('eventEntityReference'));
    $entity_bundle = $entity->bundle();
    $eventConfig = $virtualEventsCommon->getVirtualEventsConfig($entity_bundle);

    return $eventConfig;
  }

  /**
   * Get the event config object of a given event type if found.
   *
   * @return \Drupal\virtual_events\Entity\VirtualEventsConfigEntity|bool
   *   Return Config Entity if found for the given entity type,
   *   ortherwise it will return false
   */
  public function getEventSourcePlugin() {
    $sources = $this->get("sources");
    $sourceKey = $this->getEnabledSourceKey();
    $virtualEventsSourcePluginManager = \Drupal::service('plugin.manager.virtual_event_source_plugin');
    $plugin = $virtualEventsSourcePluginManager->createInstance($sourceKey);
    return $plugin;
  }

  /**
   * Check if meeting exists and active.
   */
  public function checkMeeting() {
    return $this->getEventSourcePlugin()->checkMeeting($this);
  }

  /**
   * Create a meeting on the enabled platform.
   */
  public function createMeeting() {
    return $this->getEventSourcePlugin()->createMeeting($this);
  }

  /**
   * Check the meeting on the enabled platform and recreate it it's not active.
   */
  public function reCreate() {
    if (!$this->checkMeeting()) {
      return $this->createMeeting();
    }
    return $this;
  }

}
