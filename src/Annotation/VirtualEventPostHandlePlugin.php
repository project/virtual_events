<?php

namespace Drupal\virtual_events\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Virtual event post handle plugin item annotation object.
 *
 * @see \Drupal\virtual_events\Plugin\VirtualEventPostHandlePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class VirtualEventPostHandlePlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The source types to apply this plugin to.
   *
   * @var array
   */
  public $sourceTypes;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

}
