<?php

namespace Drupal\virtual_events\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Virtual event source plugin item annotation object.
 *
 * @see \Drupal\virtual_events\Plugin\VirtualEventSourcePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class VirtualEventSourcePlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
